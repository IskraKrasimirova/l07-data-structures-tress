﻿namespace P05.MergeSortedLists
{
    public class Merge
    {
        public static int[] MergeLists(int[,] lists)
        {
            var priorityQueue = new PriorityQueue<int, int>();

            for (int i = 0; i < lists.GetLength(0); i++)
            {
                for (int j = 0; j < lists.GetLength(1); j++)
                {
                    int current = lists[i, j];

                    priorityQueue.Enqueue(current, current);
                }
            }

            List<int> orderedNumbers = new List<int>();

            while (priorityQueue.Count > 0)
            {
                orderedNumbers.Add(priorityQueue.Dequeue());
            }

            return orderedNumbers.ToArray();
        }
    }
}
