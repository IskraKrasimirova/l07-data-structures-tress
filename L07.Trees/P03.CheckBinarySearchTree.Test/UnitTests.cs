using NUnit.Framework;

namespace P03.CheckBinarySearchTree.Test
{
    public class Tests
    {
        private BinaryTree binaryTree;

        [Test]
        public void CheckBST2ReturnsTrueWhenBinaryTreeIsBST()
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node76 = new BinaryTree(76, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node76);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);

            this.binaryTree = new BinaryTree(25, node11, node52);

            Assert.That(BinaryTree.CheckBST2(binaryTree), Is.True);
        }

        [Test]
        public void CheckBST2ReturnsFalseWhenBinaryTreeIsNotBST()
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node65 = new BinaryTree(65, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node65);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);

            this.binaryTree = new BinaryTree(25, node11, node52);

            Assert.That(BinaryTree.CheckBST2(binaryTree), Is.False);
        }

        [Test]
        public void CheckBSTReturnsTrueWhenBinaryTreeIsBST()
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node76 = new BinaryTree(76, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node76);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);

            this.binaryTree = new BinaryTree(25, node11, node52);

            Assert.That(BinaryTree.CheckBST(binaryTree), Is.True);
        }

        [Test]
        public void CheckBSTReturnsFalseWhenBinaryTreeIsNotBST()
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node65 = new BinaryTree(65, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node65);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);

            this.binaryTree = new BinaryTree(25, node11, node52);

            Assert.That(BinaryTree.CheckBST(binaryTree), Is.False);
        }
    }
}